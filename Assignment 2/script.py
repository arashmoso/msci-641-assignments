import sys
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.naive_bayes import MultinomialNB
from sklearn import metrics
from copy import deepcopy
import numpy as np

def read_data(pos_file, neg_file):
    train_set = []
    labels = []
    with open(pos_file, 'r') as f:
        for line in f.readlines():
            train_set.append(" ".join(eval(line)))
            labels.append(0)
    with open(neg_file, 'r') as f:
        for line in f.readlines():
            train_set.append(" ".join(eval(line)))
            labels.append(1)
    return (train_set, labels)


def extract_freqs(train_set, valid_set, test_set, ngram):
    ngram_lower, ngram_upper = ngram
    vectorizer = CountVectorizer(ngram_range=(ngram_lower, ngram_upper))
    train_freqs = vectorizer.fit_transform(train_set)
    valid_freqs = vectorizer.transform(valid_set)
    test_freqs = vectorizer.transform(test_set)
    return train_freqs, valid_freqs, test_freqs


def train(train_freq, valid_freq, train_labels, valid_labels):
    best_alpha = 1.
    best_accuracy = 0.

    for alpha in np.arange(1.0, -0.1, -0.1):
        model = MultinomialNB(alpha=int(alpha * 10)/10., class_prior=None, fit_prior=True)
        model.fit(train_freq, train_labels)
        valid_calc_labels = model.predict(valid_freq)
        accuracy = metrics.accuracy_score(valid_labels, valid_calc_labels)
        if accuracy > best_accuracy:
            best_accuracy = accuracy
            best_alpha = alpha
            best_model = deepcopy(model)

    return (best_model, best_alpha)


def main(training_pos, training_neg, validation_pos, validation_neg, test_pos, test_neg):
    (train_set, train_labels) = read_data(training_pos, training_neg)
    (valid_set, valid_labels) = read_data(validation_pos, validation_neg)
    (test_set, test_labels) = read_data(test_pos, test_neg)

    print("text features\taccuracy(test)")
    print("-" * 30)
    for ngram in [(1,1), (2,2), (1,2)]:
        train_freqs, valid_freqs, test_freqs = extract_freqs(train_set, valid_set, test_set, ngram)
        (model, alpha) = train(train_freqs, valid_freqs, train_labels, valid_labels)
        test_calc_labels = model.predict(test_freqs)
        accuracy = metrics.accuracy_score(test_labels, test_calc_labels)
        print("{0}\t\t{1}".format(ngram, accuracy))







if __name__ == "__main__":
    main(sys.argv[1], sys.argv[2], sys.argv[3], sys.argv[4], sys.argv[5], sys.argv[6])