from glob import glob
import pickle
import logging
import multiprocessing
from gensim.models import Word2Vec
from gensim.models.phrases import Phrases, Phraser

logging.basicConfig(format="%(asctime)s: %(message)s", datefmt= '%H:%M:%S', level=logging.INFO)

FILE_PATHS = glob('../data/*[!word].csv')
SAVED_TOKENS_PATH = '../data/tokens_sentences.pkl'


def read_data(paths, force_fetch=False):
    data = list()
    try:
        assert not force_fetch
        data = pickle.load(open(SAVED_TOKENS_PATH, 'rb'))
    except Exception:
        for path in paths:
            with open(path, 'r') as f:
                for line in f.readlines():
                    data.append(eval(line))
        with open(SAVED_TOKENS_PATH, 'wb') as f:
            pickle.dump(data, f)
    finally:
        return data


# ----------------- Was not used in the final approach ----------------- #
def make_phrases(sentences, force_fetch=False):
    try:
        assert not force_fetch
        phrases = Phrases.load(fname='./phrases')
    except:
        phrases = Phrases(sentences, progress_per=5000)
        phrases.save('./phrases')
    finally:
        return phrases


def make_phraser(phrases, force_fetch=False):
    try:
        assert not force_fetch
        bigrams = Phraser.load(fname='./phraser')
    except:
        bigrams = Phraser(phrases)
        bigrams.save('./phraser')
    finally:
        return bigrams
# --------------------------------------------------------------------- #


def make_model(sentences, epochs=10, force_fetch=False):
    try:
        assert not force_fetch
        model = Word2Vec.load('./word2vec.model')
    except:
        cpu_cores = multiprocessing.cpu_count()
        model = Word2Vec(size=300,
                         min_count=20,
                         sg=1,
                         window=4,
                         workers=cpu_cores - 1,
                         negative=20,
                         sample=6e-5,
                         alpha=0.03,
                         min_alpha=0.001,
                         )
        model.build_vocab(sentences, progress_per=50000)
        model.train(sentences, total_examples=model.corpus_count, epochs=epochs, report_delay=5)
        model.init_sims(replace=True)
        model.save('./word2vec-skipgram.model')
    finally:
        return model


def main():
    sentences = read_data(FILE_PATHS)
    # bigrams = make_phraser(make_phrases(sentences))
    # sentences = bigrams[sentences]
    model = make_model(sentences, force_fetch=True, epochs=5)
    print("#"*15 + " GOOD " + "#"*15)
    print(*model.most_similar('good', topn=20), sep='\n')
    print("\n" + "#"*15 + " BAD " + "#"*15)
    print(*model.most_similar('bad', topn=20), sep='\n')


if __name__ == "__main__":
    main()
    # sentences = read_data(FILE_PATHS)
    # model = Word2Vec(
    #     sentences,
    #     size=300,
    #     workers=7,
    # )