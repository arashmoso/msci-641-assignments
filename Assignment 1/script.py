import sys
import re
import os
import random
import numpy as np

removing_punc_regex = r'[\!\"\#\$%\&\(\)\*\+\/\:\;\<\=\>\@\[\\\]\^\`\{\|\}\~\t\n]'
stopwords_file_path = os.path.dirname(os.path.realpath(__file__)) + '/../data/stopwords.json'


def normalize(text, stopwords=[]):
    text = text.lower()
    text = re.sub(removing_punc_regex, ' ', text)
    text = re.sub(r'[^0-9a-zA-Z\s]+', r' \g<0> ', text)

    text = re.sub(r'\d+', r' \g<0> ', text)

    if len(stopwords) != 0:
        text = ' ' + re.sub(" ' ", "'", text)
        for w in stopwords:
            text = re.sub(r'\s' + w + r'\s', ' ', text)
        text = re.sub("'", " ' ", text)

    text = re.sub('[ ]{2,}', ' ', text).strip()

    return text


def tokenize(text):
    return re.split(" ", text)


def load_stopwords():
    try:
        with open(stopwords_file_path, 'r') as stopwords_json:
            import json
            stopwords = json.load(stopwords_json)
            assert isinstance(stopwords, list)
            return stopwords
    except:
        with open(stopwords_file_path, 'w') as stopwords_json:
            import json
            from nltk.corpus import stopwords as sw
            stopwords = sw.words('english')
            json.dump(stopwords, stopwords_json)
            return stopwords


def get_abv_stopwords(stopwords):
    return [w for w in stopwords if '\'' in w]


def export(train_list, val_list, test_list, train_list_no_stopword, val_list_no_stopword, test_list_no_stopword, pos=True):
    np.savetxt("{0}_train.csv".format('pos' if pos else 'neg'), train_list, delimiter=",", fmt='%s')
    np.savetxt("{0}_val.csv".format('pos' if pos else 'neg'), val_list, delimiter=",", fmt='%s')
    np.savetxt("{0}_test.csv".format('pos' if pos else 'neg'), test_list, delimiter=",", fmt='%s')

    np.savetxt("{0}_train_no_stopword.csv".format('pos' if pos else 'neg'), train_list_no_stopword,
               delimiter=",", fmt='%s')
    np.savetxt("{0}_val_no_stopword.csv".format('pos' if pos else 'neg'), val_list_no_stopword,
               delimiter=",", fmt='%s')
    np.savetxt("{0}_test_no_stopword.csv".format('pos' if pos else 'neg'), test_list_no_stopword,
               delimiter=",", fmt='%s')


def main(input_file_path):
    # input_file_path = '/home/arash/Codes/Waterloo/MSCI 641/MSCI641Assignments/data/pos.txt'

    with open(input_file_path, 'r') as input_file:
        lines = input_file.readlines()

    # lines = lines[: int(len(lines)/100)]
    stopwords_list = load_stopwords()

    lines_no_stopword = []
    for idx in range(len(lines)):
        line = lines[idx]
        line_no_stopword = normalize(line, stopwords_list)
        line = normalize(line)
        lines[idx] = tokenize(line)
        lines_no_stopword.append(tokenize(line_no_stopword))

    shuffled_idxs = list(range(len(lines)))
    random.shuffle(shuffled_idxs)
    len_lines = len(shuffled_idxs)

    train_list = [lines[shuffled_idxs[i]] for i in range(0, int(len_lines * .8))]
    val_list = [lines[shuffled_idxs[i]] for i in range(int(len_lines * .8), int(len_lines * .9))]
    test_list = [lines[shuffled_idxs[i]] for i in range(int(len_lines * .9), len_lines)]

    train_list_no_stopword = [lines_no_stopword[shuffled_idxs[i]] for i in range(0, int(len_lines * .8))]
    val_list_no_stopword = [lines_no_stopword[shuffled_idxs[i]] for i in range(int(len_lines * .8), int(len_lines * .9))]
    test_list_no_stopword = [lines_no_stopword[shuffled_idxs[i]] for i in range(int(len_lines * .9), len_lines)]

    export(train_list, val_list, test_list, train_list_no_stopword, val_list_no_stopword, test_list_no_stopword, 'pos' in input_file_path)
    # export(train_list, val_list, test_list,[],[],[])

def test():
    text = "I got this for my parentsThey didn't want a large size, and this was just what they were looking for."
    print(normalize(text, load_stopwords()))


if __name__ == "__main__":
    main(sys.argv[1])
    # main('')
    # test()
